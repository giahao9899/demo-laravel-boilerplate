<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => '1 cấp độ đã được tạo.',
            'deleted' => '1 cấp độ đã được xóa.',
            'updated' => '1 cấp độ đã được cập nhập.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email' => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed' => 'The user was successfully confirmed.',
            'created' => 'Tài khoản đã được tạo thành công.',
            'deleted' => 'Tài khoản đã được xóa thành công.',
            'deleted_permanently' => 'The user was deleted permanently.',
            'restored' => 'The user was successfully restored.',
            'session_cleared' => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated' => 'Tài khoản đã được cập nhập.',
            'updated_password' => "The user's password was successfully updated.",
        ],

        'posts' => [
            'created' => 'Đã tạo một bài viết thành công.',
            'updated' => 'Đã cập nhập một bài viết thành công.',
            'deleted' => 'Đã xóa một bài viết thành công.',
        ]
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
    ],
];
