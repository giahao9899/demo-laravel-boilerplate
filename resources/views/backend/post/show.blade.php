@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.posts.management'))


@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2>{{ $post->title }}</h2>
                    <p><strong>Author: </strong>{{ $post->user->email }}</p>
                    <hr>
                    <div class="desc">
                        {!! $post->content !!}
                    </div>
                </div>
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection
