@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.posts.management'))


@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.access.posts.management') }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                        <a href="{{ route('admin.post.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="" data-original-title="Create New"><i class="fas fa-plus-circle"></i></a>
                    </div>
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>User</th>
                                <th>Active</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->user->email }}</td>
                                    <td>
                                        <a href="{{ route('admin.post.active', ['id' => $post->id]) }}">
                                            @if($post->active)
                                                <span class="badge badge-success" style="cursor:pointer">Yes</span>
                                            @else
                                                <span class="badge badge-danger" style="cursor:pointer">No</span>
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a href="{{ route('admin.post.show', ['id' => $post->id]) }}" class="btn btn-info" data-original-title="View"><i class="fas fa-eye"></i></a>
                                            <a href="{{ route('admin.post.edit', ['id' => $post->id]) }}" class="btn btn-primary" data-original-title="Edit"><i class="fas fa-edit"></i></a>
                                            <a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find('form').submit();"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                                <form action="{{ route('admin.post.destroy', ['id' => $post->id]) }}" method="GET" name="delete_item" style="display:none">
                                                    <input type="hidden" name="_method" value="delete">
                                                    <input type="hidden" name="_token" value="YOUR_CSRF_TOKEN">
                                                </form>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {!! $posts->total() !!} {{ trans_choice('labels.backend.access.posts.table.total', $posts->total()) }}
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        {!! $posts->render() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection
