@extends('backend.layouts.app')

@section('title', __('labels.backend.access.posts.management') . ' | ' . __('labels.backend.access.posts.edit'))

@section('content')
    {{ html()->form('POST', route('admin.post.update', ['id' => $post->id]))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.posts.management')
                        <small class="text-muted">@lang('labels.backend.access.posts.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" for="title">Title</label>

                        <div class="col-md-10">
                            <input class="form-control" type="text" name="title" id="title" required="" autofocus="" value="{{ $post->title }}">
                        </div><!--col-->
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" for="active">Active</label>

                        <div class="col-md-10">
                            <label class="switch switch-label switch-pill switch-primary">
                                <input class="switch-input" type="checkbox" name="active" id="active" value="1"
                                @if($post->active)
                                    checked
                                @endif
                                >
                                <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                            </label>
                        </div><!--col-->
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" for="content">Content</label>

                        <div class="col-md-10">
                            <textarea class="form-control" type="text" rows="10" name="content" id="article-ckeditor" required="">{{ $post->content }}</textarea>
                        </div><!--col-->
                    </div>
                </div>
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    <a class="btn btn-danger btn-sm" href="{{ route('admin.post.index') }}">Cancel</a>
                </div><!--col-->

                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right" type="submit">Update</button>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection

@push('after-scripts')
    <script src="{{ url('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
@endpush
