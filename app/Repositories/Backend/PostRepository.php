<?php
namespace App\Repositories\Backend;

use App\Models\Post;
use App\Repositories\BaseRepository;

class PostRepository extends BaseRepository
{

    public function model()
    {
         return Post::class;
    }

    public function getPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function active($id) {
        $post = $this->model->findOrFail($id);
        $post->active = $post->active ? 0 : 1;
        $post->save();

        return $post;
    }
}
