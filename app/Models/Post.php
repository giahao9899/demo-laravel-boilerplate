<?php

namespace App\Models;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'title',
        'slug',
        'user_id',
        'active',
        'content'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
