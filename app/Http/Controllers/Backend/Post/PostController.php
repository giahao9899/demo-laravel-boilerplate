<?php

namespace App\Http\Controllers\Backend\Post;

use App\Http\Requests\Backend\Post\StorePostRequest;
use App\Http\Requests\Backend\Post\UpdatePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\PostRepository;
use Illuminate\Support\Str;

class PostController extends Controller
{
    protected $postRepository;

    function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index() {
        return view('backend.post.index')
            ->withPosts($this->postRepository->getPaginated(10));
    }

    public function create() {
        return view('backend.post.create');
    }

    public function store(StorePostRequest $request) {
        $title = $request->title;
        $slug = Str::slug($title);
        $user_id = \Auth::id();
        $active = $request->active ?? 0;
        $content = $request->content;
        $this->postRepository->create([
            'title' => $title,
            'active' => $active,
            'slug' => $slug,
            'user_id' => $user_id,
            'content' => $content
        ]);

        return redirect()->route('admin.post.index')->withFlashSuccess(__('alerts.backend.posts.updated'));
    }

    public function show($id) {
        $post = $this->postRepository->getById($id);
        return view('backend.post.show')->withPost($post);
    }

    public function edit($id) {
        $post = $this->postRepository->getById($id);
        return view('backend.post.edit')->withPost($post);
    }

    public function update(UpdatePostRequest $request, $id) {
        $title = $request->title;
        $slug = Str::slug($title);
        $active = $request->active ?? 0;
        $content = $request->content;

        $this->postRepository->updateById($id, [
            'title' => $title,
            'slug' => $slug,
            'active' => $active,
            'content' => $content
        ]);

        return redirect()->route('admin.post.index')->withFlashSuccess(__('alerts.backend.posts.updated'));
    }

    public function destroy($id) {
        $this->postRepository->deleteById($id);
        return redirect()->route('admin.post.index')->withFlashSuccess(__('alerts.backend.posts.deleted'));
    }
}
