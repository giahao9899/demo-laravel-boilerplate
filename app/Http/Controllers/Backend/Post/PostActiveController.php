<?php

namespace App\Http\Controllers\Backend\Post;

use App\Repositories\Backend\PostRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostActiveController extends Controller
{
    protected $postRepository;

    function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function active($id) {
        $this->postRepository->active($id);
        return redirect()->back();
    }
}
