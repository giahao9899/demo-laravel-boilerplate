<?php
use App\Http\Controllers\Backend\Post\PostController;
use App\Http\Controllers\Backend\Post\PostActiveController;
// All route names are prefixed with 'admin.post'.
Route::group([
    'prefix' => 'post',
    'namespace' => 'Post',
    'middleware' => 'role:'.config('access.users.admin_role'),
], function () {
    //product CRUD
    Route::get('/', [PostController::class, 'index'])->name('post.index');
    Route::get('/create', [PostController::class, 'create'])->name('post.create');
    Route::post('/', [PostController::class, 'store'])->name('post.store');

    Route::group(['prefix' => '/{id}'], function () {
        Route::get('/', [PostController::class, 'show'])->name('post.show');
        Route::get('/edit', [PostController::class, 'edit'])->name('post.edit');
        Route::post('/edit', [PostController::class, 'update'])->name('post.update');
        Route::get('/destroy', [PostController::class, 'destroy'])->name('post.destroy');

        //active
        Route::get('/active', [PostActiveController::class, 'active'])->name('post.active');
    });
});
