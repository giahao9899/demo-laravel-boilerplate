<?php

Breadcrumbs::for('admin.post.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('labels.backend.access.posts.management'), route('admin.post.index'));
});

Breadcrumbs::for('admin.post.show', function ($trail, $id) {
    $trail->parent('admin.post.index');
    $trail->push(__('labels.backend.access.posts.show'), route('admin.post.show', ['id' => $id]));
});

Breadcrumbs::for('admin.post.create', function ($trail) {
    $trail->parent('admin.post.index');
    $trail->push(__('labels.backend.access.posts.create'), route('admin.post.create'));
});

Breadcrumbs::for('admin.post.edit', function ($trail, $id) {
    $trail->parent('admin.post.index');
    $trail->push(__('labels.backend.access.posts.edit'), route('admin.post.edit', ['id' => $id]));
});
